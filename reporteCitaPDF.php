<?php
require_once "fpdf/fpdf.php";
require_once "logica/cita.php";
require_once "logica/cliente.php";
require_once "logica/medico.php";


$pdf = new FPDF("P", "mm", "Letter");
$pdf -> SetFont("Courier", "B", 20);
$pdf -> AddPage();
$pdf -> Image('img/pagina2.jpg',0,0,220);
$pdf ->SetXY(0, 0);
$pdf->SetFillColor(0, 191, 255);
$pdf ->Rect(0,0,220,20,'F');

$pdf -> Cell(216, 20, "Cita Programada",0, 2, "C");
$pdf ->Image('img/logo.png');


$Cita = new cita($_GET["idCita"]);
$Cita->consultar();


$pdf->SetFont('Arial','',16);
$pdf->SetFillColor(0, 191, 255);
$pdf ->Rect(0,265,220,17,'F');

$Cliente = new cliente($Cita->getIdCliente());
$Cliente->consultarPDF();

$pdf -> SetFont("Courier", "B", 20);
$pdf->SetY(55);
$pdf -> Cell(170, 20, "Datos Personales",0, 2, "C");
$pdf ->Ln();

$pdf->SetY(55);
$pdf ->Image($Cliente->getFoto(),60,70,-290);

$pdf->SetY(130);
$pdf->SetX(25);
$pdf->SetFont('Arial','',17);
$pdf ->Cell(90,10,"Nombre:".$Cliente->getNombre(),0);
$pdf ->Cell(90,10,"Apellido:".$Cliente->getApellido(),0);
$pdf ->Ln();
$pdf->SetX(25);
$pdf ->Cell(90,10,"Id:".$Cliente->getIdentificacion(),0);
$pdf ->Cell(90,10,"Sexo:".$Cliente->getSexo(),0);
$pdf ->Ln();
$pdf->SetX(25);
$pdf ->Cell(90,10,"RH:".$Cliente->getRh(),0);
$pdf ->Cell(90,10,"Correo:".$Cliente->getCorreo(),0);
$pdf ->Ln();


$medico=new medico($Cita->getIdMedico());
$medico->consultar();


$pdf -> SetFont("Courier", "B", 20);
$pdf->SetY(160);
$pdf -> Cell(170, 20, "Datos Cita",0, 2, "C");
$pdf->SetY(165);
$pdf ->Ln();
$pdf->SetX(20);
$pdf->SetFont('Arial','',17);
$pdf->SetFillColor(235,236,236);
$pdf->SetTextColor(3, 3, 3);
$pdf ->Cell(90,14,"Fecha Programada: ".$Cita->getFecha_A(),1, 0 , 'C', 1);
$pdf ->Cell(90,14,"Hora Programada: ".$Cita->getHora_A(),1, 0 , 'C', 1);
$pdf ->Ln();
$pdf->SetX(20);
$pdf ->Cell(90,14,"Fecha: ".$Cita->getFecha(),1, 0 , 'C', 1);
$pdf ->Cell(90,14,"Hora: ".$Cita->getHora(),1, 0 , 'C', 1);
$pdf ->Ln();
$pdf->SetX(20);
$pdf ->Cell(180,14,"Medico",1, 0 , 'C', 1);
$pdf ->Ln();
$pdf->SetX(20);
$pdf ->Cell(90,14,"Nombre: ".$medico->getNombre(),1, 0 , 'C', 1);
$pdf ->Cell(90,14,"Apellido: ".$medico->getApellido(),1, 0 , 'C', 1);
$pdf ->Ln();
$pdf->SetX(20);
$pdf ->Cell(90,14,"Tarjeta P: ".$medico->getTarjetaProfesional(),1, 0 , 'C', 1);
$pdf ->Cell(90,14,"Correo: ".$medico->getCorreo(),1, 0 , 'C', 1);
$pdf ->Ln();

$pdf -> Output();



?>
