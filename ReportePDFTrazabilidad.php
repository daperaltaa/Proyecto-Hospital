<?php
require_once "fpdf/fpdf.php";
require_once "logica/Proceso.php";
require_once "logica/cliente.php";
require_once "logica/cita.php";
require_once "logica/medico.php";
require_once "logica/laboratorio.php";
require_once "logica/analista.php";

$pdf = new FPDF("P", "mm", "Letter");
$pdf -> SetFont("times", "B", 20);
$pdf -> AddPage();
$pdf -> Image('img/pagina2.jpg',0,0,220);
$pdf ->SetXY(0, 0);
$pdf->SetFillColor(0, 191, 255);
$pdf ->Rect(0,0,225,20,'F');

$pdf -> Cell(216, 25, "Proceso de Atencion Medica",0, 2, "C");




$Proceso=new Proceso($_GET["idCita"]);
$Proceso->consultarPDF();


$Cliente = new cliente($Proceso->getIdCliente());
$Cliente->consultarPDF();


$pdf -> SetFont("times", "B", 20);
$pdf->SetXY(25,45);
$pdf -> Cell(170, 20, "Cliente Atendido",0, 2, "C");
$pdf ->Ln();
$pdf -> SetFont("times", "B", 14);
$pdf->SetY(120);
$pdf ->Image($Cliente->getFoto(),70,70,90);
$pdf ->Ln();
$pdf->SetXY(50,120);
$pdf ->Cell(80,75,"Nombre: ".$Cliente->getNombre(),0);
$pdf ->Cell(110,75,"Apellido: ".$Cliente->getApellido(),0);
$pdf ->Ln();
$pdf->SetXY(50,130);
$pdf ->Cell(80,75,"Id: ".$Cliente->getIdentificacion(),0);
$pdf ->Cell(110,75,"Sexo: ".$Cliente->getSexo(),0);
$pdf ->Ln();
$pdf->SetXY(50,140);
$pdf ->Cell(80,75,"RH: ".$Cliente->getRh(),0);
$pdf ->Cell(110,75,"Correo: ".$Cliente->getCorreo(),0);
$pdf ->Ln();


$Cita = new cita($_GET["idCita"]);
$Cita->consultar();

$pdf -> AddPage();
$pdf -> Image('img/pagina2.jpg',0,0,220);
$pdf -> SetFont("times", "B", 20);
$pdf->SetXY(25,120);
$pdf -> Cell(170, 20, "Cita Solicitada",0, 2, "C");

$pdf ->Ln();
$pdf -> SetFont("times", "B", 14);
$pdf -> Image('img/cita.png',0,0,216);
$pdf->SetXY(30,140);
$pdf ->Cell(100,10,"Fecha: ".$Cita->getFecha_A(),0);
$pdf ->Cell(100,10,"Hora: ".$Cita->getHora_A(),0);
$pdf ->Ln();
$pdf->SetXY(30,150);
$pdf ->Cell(100,10,"Fecha/Cita: ".$Cita->getFecha(),0);
$pdf ->Cell(100,10,"Hora Cita: ".$Cita->getHora(),0);
$pdf ->Ln();


$Medico = new medico($Proceso->getIdMedico());
$Medico->consultar();

$pdf -> AddPage();
$pdf -> Image('img/pagina2.jpg',0,0,220);

$pdf -> SetFont("times", "B", 20);
$pdf->SetXY(25,45);
$pdf -> Cell(170, 20, "Medico que Atendio",0, 2, "C");
$pdf ->Ln();
$pdf ->Image($Medico->getFoto(),70,70,90);
$pdf ->Ln();
$pdf->SetY(60);
$pdf -> SetFont("times", "B", 14);
$pdf->SetXY(30,140);
$pdf ->Cell(90,10,"Nombre: ".$Medico->getNombre(),0);
$pdf ->Cell(90,10,"Apellido: ".$Medico->getApellido(),0);
$pdf ->Ln();
$pdf->SetXY(30,150);
$pdf ->Cell(90,10,"Correo: ".$Medico->getCorreo(),0);
$pdf ->Cell(90,10,"Tarjeta Profesional: ".$Medico->getTarjetaProfesional(),0);
$pdf ->Ln();



$laboratorio = new laboratorio($Proceso->getIdLaboratorio());
$laboratorio->consultarLaboratorio();

$pdf -> AddPage();
$pdf -> Image('img/pagina2.jpg',0,0,220);
$pdf -> SetFont("times", "B", 20);
$pdf->SetXY(25,35);
$pdf -> Cell(170, 20, "Resultados del laboratorio",0, 2, "C");
$pdf ->Ln();
$pdf -> SetFont("times", "B", 14);
$pdf->SetXY(55,60);
$pdf ->Cell(110,10,"Fecha: ".$laboratorio->getFecha(),1);
$pdf ->Ln();
$pdf->SetXY(55,70);
$pdf ->Cell(110,10,"Colesterol: ".$laboratorio->getColesterol(),1);
$pdf ->Ln();
$pdf->SetXY(55,80);
$pdf ->Cell(110,10,"Observacion: ".$laboratorio->getob_Colesterol(),1);
$pdf ->Ln();
$pdf->SetXY(55,90);
$pdf ->Cell(110,10,"Trigiliceridos: ".$laboratorio->getTrigliceridos(),1);
$pdf ->Ln();
$pdf->SetXY(55,100);
$pdf ->Cell(110,10,"Observacion: ".$laboratorio->getob_Trigliceridos(),1);
$pdf ->Ln();
$pdf->SetXY(55,110);
$pdf ->Cell(110,10,"Hemoglobina: ".$laboratorio->getHemoglobina(),1);
$pdf ->Ln();
$pdf->SetXY(55,120);
$pdf ->Cell(110,10,"Observacion: ".$laboratorio->getob_Hemoglobina(),1);
$pdf ->Ln();
$pdf->SetXY(55,130);
$pdf ->Cell(110,10,"Linfositos: ".$laboratorio->getLinfositos(),1);
$pdf ->Ln();
$pdf->SetXY(55,140);
$pdf ->Cell(110,10,"Observacion: ".$laboratorio->getob_Linfositos(),1);
$pdf ->Ln();
$pdf->SetXY(55,150);
$pdf ->Cell(110,10,"Glucosa: ".$laboratorio->getGlucosa(),1);
$pdf ->Ln();
$pdf->SetXY(55,160);
$pdf ->Cell(110,10,"Observacion: ".$laboratorio->getob_Glucosa(),1);
$pdf ->Ln();
$pdf->SetXY(55,170);
$pdf ->Cell(110,10,"Observaciones Generales: ".$laboratorio->getObservaciones(),1);



$analista = new analista($laboratorio->getIdAnalista());
$analista->consultar();
$pdf -> AddPage();
$pdf -> Image('img/pagina2.jpg',0,0,220);
$pdf -> SetFont("times", "B", 20);
$pdf->SetXY(25,45);
$pdf -> Cell(170, 20, "Analista Encargado",0, 2, "C");
$pdf ->Ln();
$pdf -> SetFont("times", "B", 14);
$pdf->SetY(120);
$pdf ->Image($analista->getFoto(),70,70,80);
$pdf ->Ln();
$pdf->SetXY(50,130);
$pdf ->Cell(80,75,"Nombre: ".$analista->getNombre(),0);
$pdf ->Cell(110,75,"Apellido: ".$analista->getApellido(),0);
$pdf ->Ln();
$pdf->SetXY(50,140);
$pdf ->Cell(80,75,"Correo: ".$analista->getCorreo(),0);
$pdf ->Ln();

$pdf -> Output();


?>
