<?php
require_once "persistencia/conexion.php";
require_once "persistencia/logDAO.php";

class log{
    private $idLog;
    private $actor;
    private $accion;
    private $datos;
    private $fecha;
    private $hora;
    private $Idactor;
    private $conexion;
    private $logDAO;

    public function getIdLog()
    {
        return $this->idLog;
    }

    public function getActor()
    {
        return $this->actor;                                    
    }

    public function getAccion()
    {
        return $this->accion;
    }

    public function getDatos()
    {
        return $this->datos;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function getHora()
    {
        return $this->hora;
    }

    public function getIdactor()
    {
        return $this->Idactor;
    }

    public function log($idLog="",$actor="",$accion="",$datos="",$fecha="",$hora="",$Idactor=""){
        $this->idLog=$idLog;
        $this->actor=$actor;
        $this->accion=$accion;
        $this->datos=$datos;
        $this->fecha=$fecha;
        $this->hora=$hora;
        $this->Idactor=$Idactor;
        $this -> conexion = new conexion();
        $this -> logDAO=new logDAO($this->idLog, $this->actor,$this->accion,$this->datos,$this->fecha,$this->hora,$this->Idactor);
    }

    public function insertarAdmin(){
        $this -> conexion -> abrir();
        //echo $this -> logDAO -> insertarAdministrador();
        $this -> conexion -> ejecutar($this -> logDAO -> insertarAdministrador());
        $this -> conexion -> cerrar();
    }

    public function consultarFiltroAdmin($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarFiltroAdministrador($filtro));
        $logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new log($resultado[0],$resultado[1], $resultado[2], $resultado[3], $resultado[4],$resultado[5],$resultado[6]);
            array_push($logs, $p);
        }
        $this -> conexion -> cerrar();
        return $logs;
    }

    public function consultarAdministrador(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarAdministrador());
        $logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new log($resultado[0],$resultado[1], $resultado[2], $resultado[3], $resultado[4],$resultado[5],$resultado[6]);
            array_push($logs, $p);
        }
        $this -> conexion -> cerrar();
        return $logs;
    } 
    public function consultarCliente(){
        $this -> conexion -> abrir();

        $this -> conexion -> ejecutar($this -> logDAO -> consultarCliente());
        $logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new log($resultado[0],$resultado[1], $resultado[2], $resultado[3], $resultado[4],$resultado[5],$resultado[6]);
            array_push($logs, $p);
        }
        $this -> conexion -> cerrar();
        return $logs;
    }

    public function insertarCliente(){
        $this -> conexion -> abrir();
        //echo $this -> logDAO -> insertarCliente();
        $this -> conexion -> ejecutar($this -> logDAO -> insertarCliente());
        $this -> conexion -> cerrar();
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> actor = $resultado[0];
        $this -> accion = $resultado[1];
        $this -> datos = $resultado[2];
        $this -> fecha = $resultado[3];
        $this -> hora = $resultado[4];
        $this -> Idactor = $resultado[5];
    }
    
    public function consultarLogAdmin(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarLogAdmin());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> actor = $resultado[0];
        $this -> accion = $resultado[1];
        $this -> datos = $resultado[2];
        $this -> fecha = $resultado[3];
        $this -> hora = $resultado[4];
        $this -> Idactor = $resultado[5];
    }
    
    public function consultarLogAnalista(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarLogAnalista());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> actor = $resultado[0];
        $this -> accion = $resultado[1];
        $this -> datos = $resultado[2];
        $this -> fecha = $resultado[3];
        $this -> hora = $resultado[4];
        $this -> Idactor = $resultado[5];
    }
    
    public function consultarLogMedico(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarLogMedico());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> actor = $resultado[0];
        $this -> accion = $resultado[1];
        $this -> datos = $resultado[2];
        $this -> fecha = $resultado[3];
        $this -> hora = $resultado[4];
        $this -> Idactor = $resultado[5];
    }
    

    public function consultarFiltroCliente($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarFiltroCliente($filtro));
        $logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new log($resultado[0],$resultado[1], $resultado[2], $resultado[3], $resultado[4],$resultado[5],$resultado[6]);
            array_push($logs, $p);
        }
        $this -> conexion -> cerrar();
        return $logs;
    }

    public function insertarAnalista(){
        $this -> conexion -> abrir();
        //echo $this -> logDAO -> insertarAnalista();
        $this -> conexion -> ejecutar($this -> logDAO -> insertarAnalista());
        $this -> conexion -> cerrar();
    }

    public function consultarFiltroAnalista($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarFiltroAnalista($filtro));
        $logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new log($resultado[0],$resultado[1], $resultado[2], $resultado[3], $resultado[4],$resultado[5],$resultado[6]);
            array_push($logs, $p);
        }
        $this -> conexion -> cerrar();
        return $logs;
    }

    public function consultarAnalista(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarAnalista());
        $logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new log($resultado[0],$resultado[1], $resultado[2], $resultado[3], $resultado[4],$resultado[5],$resultado[6]);
            array_push($logs, $p);
        }
        $this -> conexion -> cerrar();
        return $logs;
    }

    public function insertarMedico(){
        $this -> conexion -> abrir();
        //echo $this -> logDAO -> insertarMedico();
        $this -> conexion -> ejecutar($this -> logDAO -> insertarMedico());
        $this -> conexion -> cerrar();
    }

    public function consultarFiltroMedico($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarFiltroMedico($filtro));
        $logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new log($resultado[0],$resultado[1], $resultado[2], $resultado[3], $resultado[4],$resultado[5],$resultado[6]);
            array_push($logs, $p);
        }
        $this -> conexion -> cerrar();
        return $logs;
    }

    public function consultarMedico(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarMedico());
        $logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new log($resultado[0],$resultado[1], $resultado[2], $resultado[3], $resultado[4],$resultado[5],$resultado[6]);
            array_push($logs, $p);
        }
        $this -> conexion -> cerrar();
        return $logs;
    }
}
