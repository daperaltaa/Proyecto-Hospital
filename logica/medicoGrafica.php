<?php
require_once "persistencia/conexion.php";
require_once "persistencia/medicoGraficaDAO.php";
class medicoGrafica{
    private $Especialidad;
    private $Cantidad;
    private $conexion;
    private $medicoGraficaDAO;

    public function getEspecialidad()
    {
        return $this->Especialidad;
    }

    public function getCantidad()
    {
        return $this->Cantidad;
    }

    public function medicoGrafica($Especialidad="",$Cantidad=""){
        $this->Especialidad=$Especialidad;
        $this->Cantidad=$Cantidad;
        $this -> conexion = new conexion();
        $this -> medicoGraficaDAO = new medicoGraficaDAO($this->Especialidad,$this->Cantidad);

    }

     
    public function GraficoEspecialidades(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> medicoGraficaDAO -> GraficoEspecialidades());
        $Datos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new medicoGrafica($resultado[0],$resultado[1]);
            array_push($Datos, $p);
        }
        $this -> conexion -> cerrar();
        return $Datos;
    }

}

?>
