<?php

require_once 'persistencia/conexion.php';
require_once 'persistencia/procesoDAO.php';
class Proceso{

  private $idCita;
  private $idMedico;
  private $idCliente;
  private $idLaboratorio;
  private $idAnalista;
  private $fecha;
  private $hora;
  private $Colesterol;
  private $Trigliceridos;
  private $Hemoglobina;
  private $Linfositos;
  private $Glucosa;
  private $Observaciones;
  private $conexion;
  private $procesoDAO;

  public function getIdCita()
  {
      return $this->idCita;
  }

  public function getIdMedico()
  {
      return $this->idMedico;
  }

  public function getIdCliente()
  {
      return $this->idCliente;
  }

  public function getFecha()
  {
      return $this->fecha;
  }

  public function getHora()
  {
      return $this->hora;
  }

  public function getIdLaboratorio()
  {
      return $this -> idLaboratorio;
  }

  public function getColesterol()
  {
      return $this -> Colesterol;
  }

  public function getTrigliceridos()
  {
      return $this -> Trigliceridos;
  }

  public function getHemoglobina()
  {
      return $this -> Hemoglobina;
  }

  public function getLinfositos()
  {
      return $this -> Linfositos;
  }

  public function getGlucosa()
  {
      return $this -> Glucosa;
  }

  public function getObservaciones()
  {
      return $this -> Observaciones;
  }

  public function getIdAnalista()
  {
      return $this -> idAnalista;
  }

  public function Proceso($idCita="", $idMedico="", $idCliente="", $idLaboratorio="",$idAnalista="",$fecha="", $hora="",$Colesterol="",$Trigliceridos="",$Hemoglobina="",$Linfositos="",$Glucosa="",$Observaciones=""){

    $this -> idCita = $idCita;
    $this -> idMedico = $idMedico;
    $this -> idCliente = $idCliente;
    $this -> idLaboratorio = $idLaboratorio;
    $this -> idAnalista = $idAnalista;
    $this -> fecha = $fecha;
    $this -> hora = $hora;
    $this -> Colesterol = $Colesterol;
    $this -> Trigliceridos = $Trigliceridos;
    $this -> Hemoglobina = $Hemoglobina;
    $this -> Linfositos = $Linfositos;
    $this -> Glucosa = $Glucosa;
    $this -> Observaciones = $Observaciones;
    $this -> conexion = new conexion();
    $this -> procesoDAO = new procesoDAO($this -> idCita, $this -> idMedico, $this -> idCliente ,$this -> idLaboratorio, $this -> idAnalista,$this -> fecha, $this -> hora,$this -> Colesterol, $this -> Trigliceridos, $this -> Hemoglobina, $this -> Linfositos, $this -> Glucosa, $this -> Observaciones);
  }



  public function consultarPDF(){
      $this -> conexion -> abrir();
      //echo $this -> procesoDAO -> consultarPDF();
      $this -> conexion -> ejecutar($this -> procesoDAO -> consultarPDF());
      $this -> conexion -> cerrar();
      $resultado = $this -> conexion -> extraer();
      $this -> idCita = $resultado[0];
      $this -> idMedico = $resultado[1];
      $this -> idCliente = $resultado[2];
      $this -> fecha = $resultado[3];
      $this -> hora = $resultado[4];
      $this -> idLaboratorio = $resultado[5];
      $this -> Colesterol = $resultado[6];
      $this -> Trigliceridos = $resultado[7];
      $this -> Hemoglobina = $resultado[8];
      $this -> Linfositos = $resultado[9];
      $this -> Glucosa = $resultado[10];
      $this -> Observaciones = $resultado[11];
      $this -> idAnalista = $resultado[12];
 
      
      
      

      
  }
}










?>
