<?php
require_once "persistencia/conexion.php";
require_once "persistencia/analistaDAO.php";

class analista{

  private $idAnalista;
  private $nombre;
  private $apellido;
  private $estado;
  private $tarjetaPro;
  private $correo;
  private $clave;
  private $foto;
  private $conexion;
  private $analistaDAO;

  public function getIdAnalista(){
      return $this -> idAnalista;
  }

  public function getNombre(){
      return $this -> nombre;
  }

  public function getApellido(){
      return $this -> apellido;
  }

  public function getEstado(){
      return $this -> estado;
  }

  public function getTarjetaPro(){
      return $this -> tarjetaPro;
  }

  public function getCorreo(){
      return $this -> correo;
  }

  public function getClave(){
      return $this -> clave;
  }

  public function getFoto(){
      return $this -> foto;
  }

  public function analista($idAnalista = "", $nombre = "", $apellido = "", $estado = "", $tarjetaPro = "", $correo = "", $clave = "", $foto = ""){
    $this -> idAnalista = $idAnalista;
    $this -> nombre = $nombre;
    $this -> apellido = $apellido;
    $this -> estado = $estado;
    $this -> tarjetaPro = $tarjetaPro;
    $this -> correo = $correo;
    $this -> clave = $clave;
    $this -> foto = $foto;
    $this -> conexion = new conexion();
    $this -> analistaDAO = new analistaDAO($this -> idAnalista, $this -> nombre, $this -> apellido, $this -> estado, $this -> tarjetaPro, $this -> correo, $this -> clave, $this -> foto);
  }

  public function autenticar(){
      $this -> conexion -> abrir();
      $this -> conexion -> ejecutar($this -> analistaDAO -> autenticar());
      $this -> conexion -> cerrar();
      if ($this -> conexion -> numFilas() == 1){
          $resultado = $this -> conexion -> extraer();
          $this -> idAnalista = $resultado[0];
          $this -> estado = $resultado[1];
          return true;
      }else {
          return false;
      }
  }

  public function registrar(){
      $this -> conexion -> abrir();
      echo $this -> analistaDAO -> registrar();
      $this -> conexion -> ejecutar($this -> analistaDAO -> registrar());
      $this -> conexion -> cerrar();
  }

  public function consultar(){
      $this -> conexion -> abrir();
      //echo $this -> analistaDAO -> consultar();
      $this -> conexion -> ejecutar($this -> analistaDAO -> consultar());
      $this -> conexion -> cerrar();
      $resultado = $this -> conexion -> extraer();
      $this -> nombre = $resultado[0];
      $this -> apellido = $resultado[1];
      $this -> correo = $resultado[2];
      $this -> foto = $resultado[3];
  }


  public function cambiarEstado(){
      $this -> conexion -> abrir();
      $this -> conexion -> ejecutar($this -> analistaDAO -> cambiarEstado());
      $this -> conexion -> cerrar();
  }


  public function consultarTodos(){
      $this -> conexion -> abrir();
      $this -> conexion -> ejecutar($this -> analistaDAO -> consultarTodos());
      $analistas = array();
      while(($resultado = $this -> conexion -> extraer()) != null){
          $p = new analista($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
          array_push($analistas, $p);
      }
      $this -> conexion -> cerrar();
      return $analistas;
  }


}






?>
