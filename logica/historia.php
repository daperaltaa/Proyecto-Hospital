<?php 
require_once "persistencia/conexion.php";
require_once "persistencia/historiaDAO.php";
class historia{
    private $idHistoria;
    private $Motivo_Consulta;
    private $Antecedentes;
    private $idCliente;
    private $Fecha;
    private $Hora;
    private $Observaciones;
    private $conexion;
    private $historiaDAO;
    
     

    public function getIdHistoria()
    {
        return $this->idHistoria;
    }

    public function getMotivo_Consulta()
    {
        return $this->Motivo_Consulta;
    }

    public function getAntecedentes()
    {
        return $this->Antecedentes;
    }

    public function getIdCliente()
    {
        return $this->idCliente;
    }
    
    
    public function getFecha()
    {
        return $this->Fecha;
    }
    
    public function getHora()
    {
        return $this->Hora;
    }
    
    public function getObservaciones()
    {
        return $this->Observaciones;
    }
    
    
    
    public function historia($idHistoria="",$Motivo_Consulta="",$Antecedentes="",$idCliente="",$Fecha="",$Hora="",$Observaciones=""){
        $this->idHistoria=$idHistoria;
        $this->Motivo_Consulta=$Motivo_Consulta;
        $this->Antecedentes=$Antecedentes;
        $this->idCliente=$idCliente; 
        $this->Fecha=$Fecha;
        $this->Hora=$Hora;  
        $this->Observaciones=$Observaciones;
        $this -> conexion = new conexion();
        $this -> historiaDAO = new historiaDAO($this->idHistoria,$this->Motivo_Consulta,$this->Antecedentes, $this->idCliente,$this->Fecha,$this->Hora,$this->Observaciones);
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        //echo $this -> historiaDAO -> consultarTodos();
        $this -> conexion -> ejecutar($this -> historiaDAO -> consultarTodos());
        $historias = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new historia($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4], $resultado[5], $resultado[6]);
            array_push($historias, $p);
        }
        $this -> conexion -> cerrar();
        return $historias;
    }
    
    public function registrar(){
        $this -> conexion -> abrir();
        //echo $this -> historiaDAO -> registrar();
        $this -> conexion -> ejecutar($this -> historiaDAO -> registrar());
        $this -> conexion -> cerrar();
    }

    
    
    
    
}




?>