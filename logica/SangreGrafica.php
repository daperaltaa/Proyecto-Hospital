<?php
require_once "persistencia/conexion.php";
require_once "persistencia/SangreGraficaDAO.php";
class SangreGrafica{
    private $Sangre;
    private $Cantidad;
    private $conexion;
    private $SangreGraficaDAO;



    public function getSangre()
    {
        return $this -> Sangre;
    }

    public function getCantidad()
    {
        return $this -> Cantidad;
    }

    public function SangreGrafica($Sangre="",$Cantidad=""){
        $this->Sangre=$Sangre;
        $this->Cantidad=$Cantidad;
        $this -> conexion = new conexion();
        $this -> SangreGraficaDAO = new SangreGraficaDAO($this->Sangre,$this->Cantidad);

    }

    public function GraficoSangre(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> SangreGraficaDAO -> GraficoSangre());
        $Datos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new SangreGrafica($resultado[0],$resultado[1]);
            array_push($Datos, $p);
        }
        $this -> conexion -> cerrar();
        return $Datos;
    }

}

?>
