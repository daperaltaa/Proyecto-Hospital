<?php
require_once "persistencia/conexion.php";
require_once "persistencia/clienteDAO.php";
class cliente{
    private $idCliente;
    private $nombre;
    private $apellido;
    private $sexo;
    private $identificacion;
    private $rh;
    private $estado;
    private $correo;
    private $clave;
    private $foto;
    private $conexion;
    private $administradorDAO;



    public function getIdCliente()
    {
        return $this -> idCliente;
    }

    public function getNombre()
    {
        return $this -> nombre;
    }

    public function getApellido()
    {
        return $this -> apellido;
    }

    public function getSexo()
    {
        return $this -> sexo;
    }

    public function getIdentificacion()
    {
        return $this -> identificacion;
    }

    public function getRh()
    {
        return $this -> rh;
    }

    public function getEstado()
    {
        return $this -> estado;
    }

    public function getCorreo()
    {
        return $this -> correo;
    }

    public function getClave()
    {
        return $this -> clave;
    }

    public function getFoto()
    {
        return $this -> foto;
    }

    public function cliente($idCliente="",$nombre="",$apellido="",$sexo="",$identificacion="",$rh="",$estado="",$correo="",$clave="",$foto=""){
        $this -> idCliente=$idCliente;
        $this -> nombre=$nombre;
        $this -> apellido=$apellido;
        $this -> sexo=$sexo;
        $this -> identificacion=$identificacion;
        $this -> rh=$rh;
        $this -> estado=$estado;
        $this -> correo=$correo;
        $this -> clave=$clave;
        $this -> foto=$foto;
        $this -> conexion = new conexion();
        $this -> clienteDAO = new clienteDAO($this-> idCliente, $this-> nombre,$this-> apellido,$this-> sexo,$this-> identificacion,$this-> rh,$this-> estado,$this-> correo,$this-> clave,$this-> foto);
    }

    public function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> existeCorreo());
        $this -> conexion -> cerrar();
        return $this -> conexion -> numFilas();
    }

    public function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> registrar());
        $this -> conexion -> cerrar();
    }



    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> autenticar());
        $this -> conexion -> cerrar();
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idCliente = $resultado[0];
            $this -> estado = $resultado[1];
            return true;
        }else {
            return false;
        }
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> foto = $resultado[3];
    }

    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> consultarTodos());
        $clientes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new cliente($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5], $resultado[6], $resultado[7]);
            array_push($clientes, $p);
        }
        $this -> conexion -> cerrar();
        return $clientes;
    }

    public function consultarFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> consultarFiltro($filtro));
        $clientes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new cliente($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($clientes, $p);
        }
        $this -> conexion -> cerrar();
        return $clientes;
    }


    public function consultarPDF(){
        $this -> conexion -> abrir();
        //echo $this -> clienteDAO -> consultarPDF();
        $this -> conexion -> ejecutar($this -> clienteDAO -> consultarPDF());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> sexo = $resultado[2];
        $this -> identificacion = $resultado[3];
        $this -> rh = $resultado[4];
        $this -> correo = $resultado[5];
        $this -> foto = $resultado[6];
    }




    public function cambiarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> cambiarEstado());
        $this -> conexion -> cerrar();
    }

}

?>
