<?php
require_once "persistencia/conexion.php";
require_once "persistencia/citaDAO.php";
class cita{
    private $idCita;
    private $idMedico;
    private $idCliente;
    private $fecha;
    private $hora;
    private $fecha_A;
    private $hora_A;
    private $conexion;
    private $citaDAO;


    public function getIdCita()
    {
        return $this->idCita;
    }

    public function getIdMedico()
    {
        return $this->idMedico;
    }

    public function getIdCliente()
    {
        return $this->idCliente;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function getHora()
    {
        return $this->hora;
    }

    public function getFecha_A()
    {
        return $this -> fecha_A;
    }

    public function getHora_A()
    {
        return $this-> hora_A;
    }


    public function cita($idCita="",$idMedico="",$idCliente="",$fecha="",$hora="",$fecha_A="",$hora_A=""){
        $this->idCita=$idCita;
        $this->idMedico=$idMedico;
        $this->idCliente=$idCliente;
        $this->fecha=$fecha;
        $this->hora=$hora;
        $this->fecha_A=$fecha_A;
        $this->hora_A=$hora_A;
        $this -> conexion = new conexion();
        $this -> citaDAO = new citaDAO($this -> idCita, $this -> idMedico, $this -> idCliente, $this -> fecha, $this -> hora, $this -> fecha_A, $this -> hora_A);
    }

    public function registrar(){
        $this -> conexion -> abrir();
        //echo $this -> citaDAO -> registrar();
        $this -> conexion -> ejecutar($this -> citaDAO -> registrar());
        $this -> conexion -> cerrar();
    }

    public function consultarFiltro($filtro){
        $this -> conexion -> abrir();
        //echo $this -> citaDAO -> consultarFiltro($filtro);
        $this -> conexion -> ejecutar($this -> citaDAO -> consultarFiltro($filtro));
        $Citas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new cita($resultado[0],$resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5], $resultado[6]);
            array_push($Citas, $p);
        }
        $this -> conexion -> cerrar();
        return $Citas;
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> citaDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idCita = $resultado[0];
        $this -> idMedico = $resultado[1];
        $this -> idCliente = $resultado[2];
        $this -> fecha = $resultado[3];
        $this -> hora = $resultado[4];
        $this -> fecha_A = $resultado[5];
        $this -> hora_A = $resultado[6];
    }



    public function consultarTodos(){
        $this -> conexion -> abrir();
        //echo $this -> citaDAO -> consultarTodos();
        $this -> conexion -> ejecutar($this -> citaDAO -> consultarTodos());
        $Citas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new cita($resultado[0],$resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5], $resultado[6]);
            array_push($Citas, $p);
        }
        $this -> conexion -> cerrar();
        return $Citas;
    }




    public function consultarTodosPendientes(){
        $this -> conexion -> abrir();
        //echo $this -> citaDAO -> consultarTodosPendientes();
        $this -> conexion -> ejecutar($this -> citaDAO -> consultarTodosPendientes());
        $Citas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new cita($resultado[0],$resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5], $resultado[6]);
            array_push($Citas, $p);
        }
        $this -> conexion -> cerrar();
        return $Citas;
    }
}
?>
