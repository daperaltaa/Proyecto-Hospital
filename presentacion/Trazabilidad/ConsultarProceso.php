<?php
$Cita  = new cita();
$Citas = $Cita  -> consultarTodosPendientes();
?>

<div id="resultados">
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-dark text-center">
					<h4>Procesos de Trazabilidad</h4>
				</div>
				<div class="text-right"><?php echo count($Citas) ?> registros encontrados</div>
              	<div class="card-body">
              	 <div class="table-responsive">
					<table id="example" class="table table-striped table-bordered text-center" cellspacing="0" width="100%">
						<tr>
							<th>#</th>
							<th>Fecha</th>
							<th>Hora</th>
							<th>Cliente</th>
							<th></th>
						</tr>
						<?php
						$i=1;
						foreach($Citas as $CitaActual){
						    $Cliente = new cliente($CitaActual->getIdCliente());
						    $Cliente->consultar();
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $CitaActual -> getFecha() . "</td>";
						    echo "<td>" . $CitaActual -> getHora() . "</td>";
						    echo "<td>" . $Cliente -> getNombre()." ".$Cliente->getApellido() . "</td>";
						    ?>
						    <td><a class="btn btn-primary" href="ReportePDFTrazabilidad.php?idCita=<?php echo$CitaActual ->getIdCita()?>" target="_blank"><i class="fas fa-file-pdf"></i></a></td>
						    <?php
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				  </div>
				</div>
            </div>
		</div>
	</div>
</div>
</div>

<script>
$(document).ready(function(){
    $("#filtro").keyup(function() {
    if($(this).val().length >= 1){
	    	var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/cita/ajax/buscarCitaAjax.php") ?>&filtro=" + $(this).val();
    		$("#resultados").load(url);
        }
    });
});
</script>
