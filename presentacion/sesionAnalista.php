<?php
$analista = new analista ($_SESSION["id"]);
$analista -> consultar()
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Bienvenido Analista Clinico</h4>
				</div>
              	<div class="card-body">
              		<div class="row">
              			<div class="col-9">
							<table class="table table-hover">
								<tr> <!-- table row(fila) -->
									<th>Nombre</th><!-- th:table header -->
									<td><?php echo $analista -> getNombre()?></td>
								</tr>
								<tr>
									<th>Apellido</th>
									<td><?php echo $analista -> getApellido() ?></td>
								</tr>
								<tr>
									<th>Correo</th>
									<td><?php echo $analista -> getCorreo() ?></td>
								</tr>
							</table>
						</div>
              		</div>
            	</div>
            </div>
		</div>
	</div>
</div>
