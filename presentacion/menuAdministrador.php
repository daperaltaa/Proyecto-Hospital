<?php
$administrador  = new administrador($_SESSION["id"]);
$administrador ->consultar();
?>
<nav class="navbar navbar-expand-md navbar-dark bg-dark border-bottom">
	<a class="navbar-brand"
		href="index.php?pid=<?php echo base64_encode("presentacion/sesionAdministrador.php") ?>"><i
		class="fas fa-hospital"></i></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item dropdown active"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Administrador<i class="fas fa-user-tie"></i></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/administrador/registrarAdministrador.php") ?>">Registrar <i class="fas fa-plus-square"></i></a>
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/administrador/consultarAdministradorTodos.php") ?>">ConsultarTodos <i class="far fa-list-alt"></i></a>
				</div></li>
			<li class="nav-item dropdown active"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Medico<i class="fas fa-user-md"></i></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/medico/RegistrarMedico.php") ?>">Registrar <i class="fas fa-plus-square"></i></a>
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/medico/consultarMedico.php") ?>">ConsultarTodos <i class="far fa-list-alt"></i></a>
				</div></li>

					<li class="nav-item dropdown active"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Analista<i class="fas fa-microscope"></i></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/analista/RegistrarAnalista.php") ?>">Registrar <i class="fas fa-plus-square"></i></a>
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/analista/consultarAnalista.php") ?>">ConsultarTodos <i class="far fa-list-alt"></i></a>
				</div></li>
			<!-- <li class="nav-item"><a class="nav-link" href="#">Link</a></li> -->


			<li class="nav-item dropdown active">
			<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Logs <i class="far fa-address-book"></i></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/administrador/buscarLogAdministrador.php") ?>">Administrador <i class="fas fa-list"></i></a>
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/cliente/buscarLogCliente.php") ?>">Cliente <i class="fas fa-list"></i></a>
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/Medico/buscarLogMedico.php") ?>">Medico <i class="fas fa-list"></i></a>
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/Analista/buscarLogAnalista.php") ?>">Analista <i class="fas fa-list"></i></a>
		    </div></li>

		    <li class="nav-item dropdown active">
			<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Cliente <i class="fas fa-user"></i></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">

						<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/cliente/RegistrarCliente.php") ?>">Registrar<i class="fas fa-user-cog"></i></a>
						<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/cliente/consultarClienteTodos.php") ?>">Estados <i class="fas fa-user-cog"></i></a>
		    </div></li>


		     <li class="nav-item dropdown active">
			<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Graficos<i class="fas fa-chart-pie"></i></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">

						<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/Graficos/Graficos.php") ?>">Graficos <i class="fas fa-chart-bar"></i></i></a>

		    </div></li>


		    <li class="nav-item dropdown active">
			<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Trazabilidad <i class="fas fa-tasks"></i></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/Trazabilidad/ConsultarProceso.php") ?>">Proceso<i class="fas fa-user-plus"></i></a>


		    </div></li>

		</ul>


		<ul class="navbar-nav">
			<li class="nav-item dropdown active"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Administrador: <?php echo $administrador  -> getNombre() ?> <?php echo $administrador  -> getApellido() ?> </a>


				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/administrador/editarAdministrador.php") ?>">Editar
						Perfil <i class="fas fa-user-edit"></i></a>
				</div></li>
			<li class="nav-item active"><a class="nav-link"
				href="index.php?cerrarSesion=true">Cerrar Sesion</a></li>
		</ul>
	</div>
</nav>
