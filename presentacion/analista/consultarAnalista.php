<?php
$Analista= new analista();
$Analistas = $Analista -> consultarTodos();
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-dark text-center">
					<h4>Consultar Analista</h4>
				</div>
				<div class="text-right"><?php echo count($Analistas) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Estado</th>
							<th>Servicios</th>
						</tr>
						<?php
						$i=1;
						foreach($Analistas as $AnalistaActual){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $AnalistaActual -> getNombre()." ".$AnalistaActual -> getApellido(). "</td>";
						    echo "<td>" . (($AnalistaActual -> getEstado()==1)?"<div id='icono" . $AnalistaActual -> getIdAnalista() . "'><span class='fas fa-check-circle' data-toggle='tooltip' data-placement='left' title='Habilitado'></span></div>":(($AnalistaActual -> getEstado()==0)?"<div id='icono" . $AnalistaActual -> getIdAnalista() . "'><span class='fas fa-times-circle' data-toggle='tooltip' data-placement='left' title='Deshabilitado'></span></div>":"<span class='fas fa-ban' data-toggle='tooltip' data-placement='left' title='Inactivo'></span>")) . "</td>";
						    echo "<td><div id='accion" . $AnalistaActual -> getIdAnalista() . "'><a id='cambiarEstado" . $AnalistaActual -> getIdAnalista() . "' href='#' >" . (($AnalistaActual -> getEstado()==1)?"<span class='fas fa-user-times' data-toggle='tooltip' data-placement='left' title='Deshabilitar'></span>":(($AnalistaActual -> getEstado()==0)?"<span class='fas fa-user-check' data-toggle='tooltip' data-placement='left' title='Habilitar'></span>":"")) . "</a>";
						?>
                        <script>
                        $(document).ready(function(){
                        	$("#cambiarEstado<?php echo $AnalistaActual -> getIdAnalista() ?>").click(function(e){
                        		$('[data-toggle="tooltip"]').tooltip('hide');
                        		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/analista/ajax/cambiarEstadoAnalistaAjax.php") ?>&idAnalista=<?php echo $AnalistaActual -> getIdAnalista() ?>&nuevoEstado=<?php echo (($AnalistaActual -> getEstado()==1)?"0":"1")?>";
                        		$("#icono<?php echo $AnalistaActual -> getIdAnalista() ?>").load(url);
                        		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/analista/ajax/cambiarEstadoAnalistaAccionAjax.php") ?>&idAnalista=<?php echo $AnalistaActual -> getIdAnalista() ?>&nuevoEstado=<?php echo (($AnalistaActual -> getEstado()==1)?"0":"1")?>";
                        		$("#accion<?php echo $AnalistaActual -> getIdAnalista() ?>").load(url);
                        	});
                        });
                        </script>
						<?php
						    echo "</div></td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>
