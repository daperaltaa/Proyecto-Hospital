<?php
$cliente = new cliente ($_SESSION["id"]);
$cliente -> consultar()
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Bienvenido Cliente</h4>
				</div>
              	<div class="card-body">
              		<div class="row">
              			<div class="col-3">
              				<img src="<?php echo ($cliente -> getFoto() != "")?$cliente -> getFoto():"http://icons.iconarchive.com/icons/custom-icon-design/silky-line-user/512/user2-2-icon.png"; ?>" width="100%" class="img-thumbnail"<!--img-thumbnail:coloca un borde a la imagen  -->
              			</div>
              			<div class="col-9">
							<table class="table table-hover">
								<tr> <!-- table row(fila) -->
									<th>Nombre</th><!-- th:table header -->
									<td><?php echo $cliente -> getNombre()?></td>
								</tr>
								<tr>
									<th>Apellido</th>
									<td><?php echo $cliente -> getApellido() ?></td>
								</tr>
								<tr>
									<th>Correo</th>
									<td><?php echo $cliente -> getCorreo() ?></td>
								</tr>
							</table>
						</div>
              		</div>
            	</div>
            </div>
		</div>
	</div>
</div>
