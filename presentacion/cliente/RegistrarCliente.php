<?php
$nombre = "";
if (isset($_POST["nombre"])) {
    $nombre = $_POST["nombre"];
}
$apellido = "";
if (isset($_POST["apellido"])) {
    $apellido = $_POST["apellido"];
}
$correo = "";
if (isset($_POST["correo"])) {
    $correo = $_POST["correo"];
}
$sexo = "";
if (isset($_POST["sexo"])) {
    $sexo = $_POST["sexo"];
}
$identificacion = "";
if (isset($_POST["identificacion"])) {
    $identificacion = $_POST["identificacion"];
}
$rh = "";
if (isset($_POST["rh"])) {
    $rh = $_POST["rh"];
}
$clave = "";
if (isset($_POST["clave"])) {
    $clave = $_POST["clave"];
}

if (isset($_POST["crear"])) {

  if ($_FILES["imagen"]["name"] != "") {
        $rutaLocal = $_FILES["imagen"]["tmp_name"];
        $tipo = $_FILES["imagen"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "img/" . $tiempo->getTimestamp() . (($tipo == "image/png") ? ".png" : ".jpg");
        copy($rutaLocal, $rutaRemota);
        $cliente=new cliente("",$nombre,$apellido,$sexo,$identificacion, $rh,"",$correo,$clave,$rutaRemota);
        $cliente->registrar();

    }else {
        $cliente=new cliente("",$nombre,$apellido,$sexo,$identificacion, $rh,"",$correo,$clave);
        $cliente->registrar();
    }

    date_default_timezone_set("America/Bogota");
    $fechaRealizado=date("Y-m-d");
    $horaRealizado=date("H:i:s");
    $log = new log("","Administrador","Registro Cliente","Nombre:".$nombre."\n Apellido:".$apellido."\n Correo:".$correo."\nSexo:".$sexo,$fechaRealizado,$horaRealizado,$_SESSION["id"]);
    $log ->insertarAdmin();
}
?>

<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Registrar Cliente</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["crear"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos ingresados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/cliente/RegistrarCliente.php") ?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nombre</label>
							<input type="text" name="nombre" class="form-control" value="<?php echo $nombre ?>" required>
						</div>
						<div class="form-group">
							<label>Apellido</label>
							<input type="text" name="apellido" class="form-control" value="<?php echo $apellido ?>" required>
						</div>
            <div class="form-group">
							<label>Sexo</label>
							<br>
              <select name="sexo" class="form-control">
                <option value="<?php echo $sexo = "Femenino"?>">Femenino</option>
                <option value="<?php echo $sexo = "Masculino"?>">Masculino</option>
              </select>
						</div>
            <div class="form-group">
							<label>Identificacion</label>
							<input type="text" name="identificacion" class="form-control" value="<?php echo $identificacion ?>" required>
						</div>
            <div class="form-group">
							<label>RH</label>
							<input type="text" name="rh" class="form-control" value="<?php echo $rh ?>" required>
						</div>
						<div class="form-group">
							<label>Correo</label>
							<input type="mail" name="correo" class="form-control" value="<?php echo $correo ?>" required>
						</div>
						<div class="form-group">
							<label>Clave</label>
							<input type="password" name="clave" class="form-control" value="<?php echo $clave ?>" required>
						</div>
            <div class="form-group">
							<label>Foto</label>
							<input type="file" name="imagen" class="form-control" value="<?php echo $imagen ?>">
						</div>

						<button type="submit" name="crear" class="btn btn-dark">Registrar <i class="fas fa-plus-circle"></i></button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>
