<?php
$filtro = $_GET["filtro"];
$Cliente = new cliente();
$Clientes = $Cliente -> consultarFiltro($filtro);
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Consultar Historia</h4>
				</div>
				<div class="text-right"><?php echo count($Clientes) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Apellido</th>
							<th>Opciones</th>
							<th>PDF</th>
						</tr>
						<?php
						$i=1;
						foreach($Clientes as $ClienteActual){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $ClienteActual -> getNombre() . "</td>";
						    echo "<td>" . $ClienteActual -> getApellido() . "</td>";
						    echo "<td><a href='index.php?pid=". base64_encode("presentacion/cliente/editarHistoria.php") . "&idCliente=" . $ClienteActual -> getIdCliente() . "' data-toggle='tooltip' data-placement='left' title='Editar'><span class='fas fa-edit'></span></a>
                                  <a href='index.php?pid=". base64_encode("presentacion/cliente/verHistoria.php") . "&idCliente=" . $ClienteActual -> getIdCliente() . "' data-toggle='tooltip' data-placement='left' title='Editar'><span class='fas fa-search-plus'></span></a>
                             </td>";
						    ?>
						    <td><a class="btn btn-primary" href="PDFhistoria.php?idCliente=<?php echo $ClienteActual -> getIdCliente()?>" target="_blank"><i class="fas fa-file-pdf"></i></a></td>
						    <?php
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>
