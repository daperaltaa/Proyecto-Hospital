<?php
$correo = $_POST["correo"];
$clave = $_POST["clave"];
$administrador  = new administrador("","","","",$correo, $clave,"");
$cliente = new cliente("", "", "", "", "","","",$correo, $clave,"");
$analista = new analista("","","","","",$correo, $clave,"");
$medico = new medico("","","","","",$correo,$clave,"","");

date_default_timezone_set("America/Bogota");
$fecha=date("Y-m-d");
$hora=date("H:i:s");

if($administrador  -> autenticar()){
    $_SESSION["id"] = $administrador  -> getIdAdministrador();
    $_SESSION["rol"] = "Administrador";
    $log = new log("","Administrador","Log in","Correo:".$administrador->getCorreo(),$fecha,$hora,$administrador  -> getIdAdministrador());
    $log ->insertarAdmin();
    header("Location: index.php?pid=" . base64_encode("presentacion/sesionAdministrador.php"));

}else if($cliente -> autenticar()){
    if($cliente -> getEstado() == -1){
        header("Location: index.php?error=2");
    }else if($cliente -> getEstado() == 0){
        header("Location: index.php?error=3");
    }else{
        $_SESSION["id"] = $cliente -> getIdCliente();
        $_SESSION["rol"] = "Cliente";
        $log = new log("","Cliente","Log in","Correo:".$administrador->getCorreo(),$fecha,$hora,$cliente -> getIdCliente());
        $log ->insertarCliente();
        header("Location: index.php?pid=" . base64_encode("presentacion/sesionCliente.php"));
      }
    }else if($analista  -> autenticar()){
      if($analista -> getEstado() == -1){
          header("Location: index.php?error=2");
      }else if($analista -> getEstado() == 0){
          header("Location: index.php?error=3");
      }else{
        $_SESSION["id"] = $analista  -> getIdAnalista();
        $_SESSION["rol"] = "Analista";
        $log = new log("","Analista","Log in","Correo:".$analista->getCorreo(),$fecha,$hora,$analista  -> getIdAnalista());
        $log ->insertarAnalista();
        header("Location: index.php?pid=" . base64_encode("presentacion/sesionAnalista.php"));
      }
    }else if($medico  -> autenticar()){
        if($medico -> getEstado() == -1){
          header("Location: index.php?error=2");
      }else if($medico -> getEstado() == 0){
          header("Location: index.php?error=3");
      }else{
        $_SESSION["id"] = $medico  -> getIdMedico();
        $_SESSION["rol"] = "Medico";
        $log = new log("","Medico","Log in","Correo:".$medico->getCorreo(),$fecha,$hora,$medico  -> getIdMedico());
        $log ->insertarMedico();
        header("Location: index.php?pid=" . base64_encode("presentacion/sesionMedico.php"));
      }
}else{
    header("Location: index.php?error=1");
}
?>
