<header>
  <nav class="navbar navbar-expand-md navbar-dark bg-dark border-bottom">
    <a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php") ?>"><i class="fas fa-clinic-medical"></i></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">

        </li>
        <li class="nav-item">

        </li>
        <li class="nav-item">

        </li>
      </ul>
      <div class="form-inline mt-2 mt-md-0">
          <button class="btn btn-outline-light my-2 my-sm-0" data-toggle="modal" data-target="#login">Ingresar</button>

          <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="login1" aria-hidden="true">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">
                <div class="modal-header bg-">
                  <h5 class="modal-title" id="log">Ingresar</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form class="form-signin" action="index.php?pid=<?php echo base64_encode("presentacion/autenticar.php") ?>" method="post">
                <div class="modal-body text-center">
                              <img src="img/avatar.jpg" class="rounded mx-auto d-block" height="180px" >
                              <br>
                                <input type="email" name="correo"  class="form-control"
                                  placeholder="Correo" required>
                                <input name="clave" type="password" class="form-control"
                                  placeholder="Clave" required>
                              <br>
                              <?php
                              if(isset($_GET["error"]) && $_GET["error"]==1){
                                  echo "<div class=\"alert alert-danger\" role=\"alert\">Error de correo o clave</div>";
                              }else if(isset($_GET["error"]) && $_GET["error"]==2){
                                  echo "<div class=\"alert alert-danger\" role=\"alert\">Su cuenta no ha sido activada</div>";
                              }else if(isset($_GET["error"]) && $_GET["error"]==3){
                                  echo "<div class=\"alert alert-danger\" role=\"alert\">Su cuenta ha sido inhabilitada</div>";
                              }
                              ?>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">cerrar</button>
                  <button type="submit" name="ingresar"  class="form-control btn btn-info"><h5>Ingresar <i class="fas fa-sign-in-alt"></i></h5></button>
                </div>
                </form>
              </div>
            </div>
          </div>
      </div>
    </div>
  </nav>
</header>
