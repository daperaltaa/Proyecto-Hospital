<?php
$analista  = new analista($_SESSION["id"]);
$analista ->consultar();
?>
<nav class="navbar navbar-expand-md navbar-dark bg-dark border-bottom">
	<a class="navbar-brand"
		href="index.php?pid=<?php echo base64_encode("presentacion/sesionAnalista.php") ?>"><i
		class="fas fa-hospital"></i></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<!-- <li class="nav-item"><a class="nav-link" href="#">Link</a></li> -->
			<li class="nav-item dropdown active"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Pacientes <i class="fas fa-microscope"></i></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">

					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/cliente/consultarClienteLab.php") ?>"> Ver Laboratorios <i class="fas fa-tablets"></i></a>

						<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/Trazabilidad/TrazabilidadConsultar.php") ?>">Pendientes<i class="fas fa-vials"></i></a>
				</div></li>


		</ul>


		<ul class="navbar-nav">
			<li class="nav-item dropdown active"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Analista: <?php echo $analista  -> getNombre() ?> <?php echo $analista  -> getApellido() ?> </a>


				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/analista/editarAnalista.php") ?>">Editar
						Perfil <i class="fas fa-user-edit"></i></a>
				</div></li>
			<li class="nav-item active"><a class="nav-link"
				href="index.php?cerrarSesion=true">Cerrar Sesion</a></li>
		</ul>
	</div>
</nav>
