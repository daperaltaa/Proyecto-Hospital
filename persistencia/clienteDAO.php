<?php
class clienteDAO{
    private $idCliente;
    private $nombre;
    private $apellido;
    private $sexo;
    private $identificacion;
    private $rh;
    private $estado;
    private $correo;
    private $clave;
    private $foto;

    public function clienteDAO($idCliente="",$nombre="",$apellido="",$sexo="",$identificacion="",$rh="",$estado="",$correo="",$clave="",$foto=""){
        $this -> idCliente = $idCliente;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> sexo = $sexo;
        $this -> identificacion = $identificacion;
        $this -> rh = $rh;
        $this -> estado = $estado;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
    }

    public function existeCorreo(){
        return "select correo
                from cliente
                where correo = '" . $this -> correo .  "'";
    }


    public function registrar(){
        return "insert into cliente (Nombre,Apellido, sexo, identificacion, rh, estado, correo, clave, foto)
                values ('" . $this -> nombre . "', '" .$this -> apellido . "','" .$this -> sexo . "','" .$this -> identificacion . "', '" . $this -> rh . "' , '" .'1'. "', '" .$this -> correo . "',  '" . md5($this -> clave) ."', '" .$this -> foto . "')";
    }


    public function verificarCodigoActivacion($codigoActivacion){
        return "select idCliente
                from cliente
                where correo = '" . $this -> correo .  "' and codigoActivacion = '" . md5($codigoActivacion) . "'";
    }

    public function activar(){
        return "update cliente
                set estado = '1'
                where correo = '" . $this -> correo .  "'";
    }

    public function autenticar(){
        return "select idCliente, estado
                from cliente
                where correo = '" . $this -> correo .  "' and clave = '" . md5($this -> clave) . "'";
    }

    public function consultar(){
        return "select Nombre, Apellido, correo, foto
                from cliente
                where idcliente = '" . $this -> idCliente .  "'";
    }

    public function consultarTodos(){
        return "select idCliente, Nombre, Apellido, sexo, identificacion, rh, estado, correo
                from cliente";
    }

    public function consultarFiltro($filtro){
        return "select idCliente, Nombre, Apellido, identificacion
                from cliente
                where Nombre like '%" . $filtro . "%' or Apellido like '" . $filtro . "%' or identificacion like '" . $filtro . "%'";
    }


    public function consultarPDF(){
        return "select Nombre, Apellido,sexo,identificacion,rh,correo, foto
                from cliente
                where idCliente = '" . $this -> idCliente .  "'";
    }


    public function cambiarEstado(){
        return "update cliente
                set estado = '" . $this -> estado . "'
                where idCliente = '" . $this -> idCliente .  "'";
    }


}
