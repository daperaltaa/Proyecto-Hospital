-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-08-2020 a las 18:10:13
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `final`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `idAdministrador` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Apellido` varchar(45) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `clave` varchar(45) DEFAULT NULL,
  `foto` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`idAdministrador`, `Nombre`, `Apellido`, `estado`, `correo`, `clave`, `foto`) VALUES
(3, 'Josue', 'Castro', 1, 'josue06castp@gmail.com', '202cb962ac59075b964b07152d234b70', NULL),
(4, 'Diego', 'Peralta', 1, 'dperalta@gmail.com', '202cb962ac59075b964b07152d234b70', 'img/1598333574.jpg'),
(5, 'Alejandro', 'Rodriguez', 0, 'alejandro@gmail.com', '202cb962ac59075b964b07152d234b70', 'img/1598413940.jpg'),
(6, 'Andrea', 'Cardenas', 0, 'ac@gmail.com', '202cb962ac59075b964b07152d234b70', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `analista_clinico`
--

CREATE TABLE `analista_clinico` (
  `idAnalista` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Apellido` varchar(45) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  `tarjetaPro` varchar(45) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `clave` varchar(45) DEFAULT NULL,
  `foto` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `analista_clinico`
--

INSERT INTO `analista_clinico` (`idAnalista`, `Nombre`, `Apellido`, `estado`, `tarjetaPro`, `correo`, `clave`, `foto`) VALUES
(5, 'Covid', '19', '1', '987654321', 'covid@19.com', '202cb962ac59075b964b07152d234b70', 'img/1598368092.jpg'),
(6, 'Camilo', 'Zuñiga', '1', '345312377', 'camilo@gmail.com', '202cb962ac59075b964b07152d234b70', ''),
(7, 'Marcela', 'Castillo', '0', '147852369', 'marcela@gmail.com', '202cb962ac59075b964b07152d234b70', 'img/1598414515.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cita`
--

CREATE TABLE `cita` (
  `idCita` int(11) NOT NULL,
  `Fecha` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `fecha_A` date NOT NULL,
  `hora_A` time NOT NULL,
  `Cliente_idCliente` int(11) NOT NULL,
  `Medico_idMedico` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cita`
--

INSERT INTO `cita` (`idCita`, `Fecha`, `hora`, `fecha_A`, `hora_A`, `Cliente_idCliente`, `Medico_idMedico`) VALUES
(1, '2020-08-27', '13:00:00', '2020-08-25', '10:49:04', 1, 1),
(4, '2020-08-29', '14:00:00', '2020-08-25', '23:02:53', 6, 3),
(7, '2020-08-28', '10:00:00', '2020-08-26', '10:02:46', 9, 1),
(8, '2020-08-28', '12:00:00', '2020-08-26', '10:12:15', 10, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idCliente` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Apellido` varchar(45) DEFAULT NULL,
  `sexo` varchar(45) DEFAULT NULL,
  `identificacion` int(12) DEFAULT NULL,
  `rh` varchar(45) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `clave` varchar(45) DEFAULT NULL,
  `foto` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idCliente`, `Nombre`, `Apellido`, `sexo`, `identificacion`, `rh`, `estado`, `correo`, `clave`, `foto`) VALUES
(1, 'Maria', 'Castañeda', 'Femenino', 1442578963, 'O+', '1', 'maria@gmail.com', '202cb962ac59075b964b07152d234b70', 'img/1598328221.jpg'),
(2, 'Esteban', 'Castro', 'Masculino', 2147483647, 'O+', '1', 'esteban@gmail.com', '202cb962ac59075b964b07152d234b70', 'img/1598329601.jpg'),
(6, 'Ana', 'Gomez', 'Femenino', 1123658957, 'B+', '1', 'ana@gmail.com', '202cb962ac59075b964b07152d234b70', 'img/1598414548.jpg'),
(9, 'Daniela', 'Rodriguez', 'Femenino', 1142365879, 'A+', '1', 'daniela@gmail.com', '202cb962ac59075b964b07152d234b70', 'img/1598454130.jpg'),
(10, 'Camila', 'Peralta', 'Femenino', 2147483647, 'A+', '1', 'camila@gmail.com', '202cb962ac59075b964b07152d234b70', 'img/1598454666.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historia`
--

CREATE TABLE `historia` (
  `idHistoria` int(11) NOT NULL,
  `Motivo_Consulta` text DEFAULT NULL,
  `Antecedentes` text DEFAULT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `Cliente_idCliente` int(11) NOT NULL,
  `observaciones` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `historia`
--

INSERT INTO `historia` (`idHistoria`, `Motivo_Consulta`, `Antecedentes`, `fecha`, `hora`, `Cliente_idCliente`, `observaciones`) VALUES
(1, 'Dolor de Cabeza', 'Migraña aguda', '2020-08-25', '22:36:56', 1, 'Presenta sintomas de migraña'),
(2, 'Nauseas', 'Dolor de estomago', '2020-08-25', '23:04:43', 6, 'Puede tener cancer'),
(4, 'Dolor de cabeza', 'Migraña', '2020-08-26', '10:03:54', 9, 'Se encuentra bien'),
(5, 'Dolor de estomago', 'Ninguno', '2020-08-26', '10:13:17', 10, 'El paciente esta bien');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `laboratorio_sangre`
--

CREATE TABLE `laboratorio_sangre` (
  `idLaboratorio` int(11) NOT NULL,
  `Fecha` date DEFAULT NULL,
  `Colesterol` varchar(45) DEFAULT NULL,
  `ob_Colesterol` text NOT NULL,
  `Trigliceridos` varchar(45) DEFAULT NULL,
  `ob_Trigliceridos` text NOT NULL,
  `Hemoglobina` varchar(45) DEFAULT NULL,
  `ob_Hemoglobina` text NOT NULL,
  `Linfositos` varchar(45) DEFAULT NULL,
  `ob_Linfositos` text NOT NULL,
  `Glucosa` varchar(45) DEFAULT NULL,
  `ob_Glucosa` text NOT NULL,
  `observaciones` varchar(45) DEFAULT NULL,
  `Cliente_idCliente` int(11) NOT NULL,
  `Analista_Clinico_idAnalista` int(11) NOT NULL,
  `Cita_idCita` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `laboratorio_sangre`
--

INSERT INTO `laboratorio_sangre` (`idLaboratorio`, `Fecha`, `Colesterol`, `ob_Colesterol`, `Trigliceridos`, `ob_Trigliceridos`, `Hemoglobina`, `ob_Hemoglobina`, `Linfositos`, `ob_Linfositos`, `Glucosa`, `ob_Glucosa`, `observaciones`, `Cliente_idCliente`, `Analista_Clinico_idAnalista`, `Cita_idCita`) VALUES
(6, '2020-08-25', '85', 'Alto colesterol', '64', 'sin observaciones', '34', 'Baja hemoglobina', '56', 'Sin observaciones', '60', 'Glucosa estable', 'El paciente esta mal', 1, 5, 1),
(9, '2020-08-25', '20', 'Alto colesterol', '31', 'sin observaciones', '10', 'Baja hemoglobina', '30', 'Sin observaciones', '55', 'Glucosa estable', 'fxfcasdfzxfsdfasfa', 6, 5, 4),
(10, '2020-08-26', '20', 'Estable', '50', 'sin observaciones', '40', 'Sin observaciones', '13', 'Sin observaciones', '50', 'Glucosa estable', 'El paciente esta bien', 9, 5, 7),
(11, '2020-08-26', '40', 'Estable', '20', 'sin observaciones', '10', 'Sin observaciones', '1', 'Sin observaciones', '20', 'Glucosa estable', 'El paciente se encuentra bien', 10, 5, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logadministrador`
--

CREATE TABLE `logadministrador` (
  `idLogAdministrador` int(11) NOT NULL,
  `actor` varchar(45) DEFAULT NULL,
  `accion` varchar(45) DEFAULT NULL,
  `datos` varchar(200) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `Administrador_idAdministrador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `logadministrador`
--

INSERT INTO `logadministrador` (`idLogAdministrador`, `actor`, `accion`, `datos`, `fecha`, `hora`, `Administrador_idAdministrador`) VALUES
(12, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-25', '00:32:32', 3),
(13, 'Administrador', 'Registro Administrador', 'Nombre:Diego\n Apellido:Peralta\n Correo:dperalta@gmail.com', '2020-08-25', '00:32:54', 3),
(14, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-25', '09:56:55', 3),
(15, 'Administrador', 'Registro Analista', 'Nombre:\n Apellido:\n Correo:\nTarjeta Profesional:987654321', '2020-08-25', '09:57:56', 3),
(16, 'Administrador', 'Registro Analista', 'Nombre:\n Apellido:\n Correo:\nTarjeta Profesional:987654321', '2020-08-25', '10:04:04', 3),
(17, 'Administrador', 'Registro Analista', 'Nombre:\n Apellido:\n Correo:\nTarjeta Profesional:987654321', '2020-08-25', '10:06:48', 3),
(18, 'Administrador', 'Registro Analista', 'Nombre:Covid\n Apellido:19\n Correo:\nTarjeta Profesional:987654321', '2020-08-25', '10:07:40', 3),
(19, 'Administrador', 'Registro Analista', 'Nombre:Covid\n Apellido:19\n Correo:covid@19.com\nTarjeta Profesional:987654321', '2020-08-25', '10:08:12', 3),
(20, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-25', '13:04:21', 3),
(21, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-25', '19:15:03', 3),
(22, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-25', '19:18:08', 3),
(23, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-25', '19:20:09', 3),
(24, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-25', '19:20:40', 3),
(25, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-25', '22:30:01', 3),
(26, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-25', '22:42:51', 3),
(27, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-25', '22:49:20', 3),
(28, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-25', '22:49:34', 3),
(29, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-25', '22:50:14', 3),
(30, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-25', '22:51:06', 3),
(31, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-25', '22:51:51', 3),
(32, 'Administrador', 'Registro Administrador', 'Nombre:Alejandro\n Apellido:Rodriguez\n Correo:alejandro@gmail.com', '2020-08-25', '22:52:20', 3),
(33, 'Administrador', 'Registro Medico', 'Nombre:Andres\n Apellido:Melendez\n Correo:andres@gmail.com\nTarjeta:312342317', '2020-08-25', '22:52:52', 3),
(34, 'Administrador', 'Registro Analista', 'Nombre:Camilo\n Apellido:Zuñiga\n Correo:camilo@gmail.com\nTarjeta Profesional:345312377', '2020-08-25', '22:53:18', 3),
(35, 'Administrador', 'Registro Cliente', 'Nombre:Juan\n Apellido:gomez\n Correo:juan@gmail.com\nSexo:Masculino', '2020-08-25', '22:53:59', 3),
(36, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-25', '22:57:37', 3),
(37, 'Administrador', 'Registro Cliente', 'Nombre:Juan\n Apellido:Gomez\n Correo:juan@gmail.com\nSexo:Masculino', '2020-08-25', '22:58:06', 3),
(38, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-25', '23:00:27', 3),
(39, 'Administrador', 'Registro Administrador', 'Nombre:Andrea\n Apellido:Cardenas\n Correo:ac@gmail.com', '2020-08-25', '23:00:46', 3),
(40, 'Administrador', 'Registro Medico', 'Nombre:Jorge\n Apellido:Melendez\n Correo:jorge@gmail.com\nTarjeta:3453123434', '2020-08-25', '23:01:21', 3),
(41, 'Administrador', 'Registro Analista', 'Nombre:Marcela\n Apellido:Castillo\n Correo:marcela@gmail.com\nTarjeta Profesional:147852369', '2020-08-25', '23:01:55', 3),
(42, 'Administrador', 'Registro Cliente', 'Nombre:Ana\n Apellido:Gomez\n Correo:ana@gmail.com\nSexo:Femenino', '2020-08-25', '23:02:28', 3),
(43, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-25', '23:04:02', 3),
(44, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-25', '23:11:40', 3),
(45, 'Administrador', 'Registro Cliente', 'Nombre:Dana\n Apellido:Cabrera\n Correo:dana@gmail.com\nSexo:Femenino', '2020-08-25', '23:12:09', 3),
(46, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-25', '23:15:41', 3),
(47, 'Administrador', 'Log in', 'Correo:dperalta@gmail.com', '2020-08-25', '23:23:53', 4),
(48, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-25', '23:38:07', 3),
(49, 'Administrador', 'Log in', 'Correo:dperalta@gmail.com', '2020-08-25', '23:47:55', 4),
(50, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-25', '23:53:29', 3),
(51, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-25', '23:58:45', 3),
(52, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-26', '00:00:42', 3),
(53, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-26', '00:02:18', 3),
(54, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-26', '09:56:26', 3),
(55, 'Administrador', 'Registro Cliente', 'Nombre:Daniela\n Apellido:Rodriguez\n Correo:daniela@gmail.com\nSexo:Femenino', '2020-08-26', '09:58:19', 3),
(56, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-26', '10:01:14', 3),
(57, 'Administrador', 'Registro Cliente', 'Nombre:Daniela\n Apellido:Rodriguez\n Correo:daniela@gmail.com\nSexo:Femenino', '2020-08-26', '10:02:10', 3),
(58, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-26', '10:06:05', 3),
(59, 'Administrador', 'Log in', 'Correo:dperalta@gmail.com', '2020-08-26', '10:09:08', 4),
(60, 'Administrador', 'Registro Cliente', 'Nombre:Camila\n Apellido:Peralta\n Correo:camila@gmail.com\nSexo:Femenino', '2020-08-26', '10:11:06', 4),
(61, 'Administrador', 'Log in', 'Correo:dperalta@gmail.com', '2020-08-26', '10:15:10', 4),
(62, 'Administrador', 'Log in', 'Correo:dperalta@gmail.com', '2020-08-26', '10:18:33', 4),
(63, 'Administrador', 'Registro Cliente', 'Nombre:James\n Apellido:Rodriguez\n Correo:james@gmail.com\nSexo:Masculino', '2020-08-26', '10:20:53', 4),
(64, 'Administrador', 'Log in', 'Correo:dperalta@gmail.com', '2020-08-26', '10:24:42', 4),
(65, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-26', '10:33:21', 3),
(66, 'Administrador', 'Registro Cliente', 'Nombre:James\n Apellido:Rodriguez\n Correo:james@gmail.com\nSexo:Masculino', '2020-08-26', '10:33:52', 3),
(67, 'Administrador', 'Log in', 'Correo:josue06castp@gmail.com', '2020-08-26', '10:34:48', 3),
(68, 'Administrador', 'Log in', 'Correo:dperalta@gmail.com', '2020-08-26', '10:38:01', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `loganalista_clinico`
--

CREATE TABLE `loganalista_clinico` (
  `idAnalista_Clinico` int(11) NOT NULL,
  `actor` varchar(45) DEFAULT NULL,
  `accion` varchar(45) DEFAULT NULL,
  `datos` varchar(200) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `Analista_Clinico_idAnalista` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `loganalista_clinico`
--

INSERT INTO `loganalista_clinico` (`idAnalista_Clinico`, `actor`, `accion`, `datos`, `fecha`, `hora`, `Analista_Clinico_idAnalista`) VALUES
(1, 'Analista', 'Log in', 'Correo:covid@19.com', '2020-08-25', '10:08:30', 5),
(2, 'Analista', 'Log in', 'Correo:covid@19.com', '2020-08-25', '10:50:46', 5),
(3, 'Analista', 'Log in', 'Correo:covid@19.com', '2020-08-25', '19:29:49', 5),
(4, 'Analista', 'Log in', 'Correo:covid@19.com', '2020-08-25', '22:39:14', 5),
(5, 'Analista', 'Log in', 'Correo:covid@19.com', '2020-08-25', '22:49:29', 5),
(6, 'Analista', 'Log in', 'Correo:covid@19.com', '2020-08-25', '23:03:39', 5),
(7, 'Analista', 'Log in', 'Correo:marcela@gmail.com', '2020-08-25', '23:05:37', 7),
(8, 'Analista', 'Registro Laboratorio', 'Colesterol:50\n Trigliceridos:12\n Hemoglobina:', '2020-08-25', '23:06:41', 7),
(9, 'Analista', 'Log in', 'Correo:covid@19.com', '2020-08-25', '23:13:10', 5),
(10, 'Analista', 'Log in', 'Correo:covid@19.com', '2020-08-25', '23:14:10', 5),
(11, 'Analista', 'Registro Laboratorio', 'Colesterol:40\n Trigliceridos:20\n Hemoglobina:', '2020-08-25', '23:14:43', 5),
(12, 'Analista', 'Log in', 'Correo:covid@19.com', '2020-08-25', '23:43:27', 5),
(13, 'Analista', 'Log in', 'Correo:covid@19.com', '2020-08-25', '23:46:02', 5),
(14, 'Analista', 'Registro Laboratorio', 'Colesterol:20\n Trigliceridos:31\n Hemoglobina:', '2020-08-25', '23:46:42', 5),
(15, 'Analista', 'Log in', 'Correo:covid@19.com', '2020-08-26', '00:02:52', 5),
(16, 'Analista', 'Log in', 'Correo:covid@19.com', '2020-08-26', '10:04:35', 5),
(17, 'Analista', 'Registro Laboratorio', 'Colesterol:20\n Trigliceridos:50\n Hemoglobina:', '2020-08-26', '10:05:28', 5),
(18, 'Analista', 'Log in', 'Correo:covid@19.com', '2020-08-26', '10:13:55', 5),
(19, 'Analista', 'Registro Laboratorio', 'Colesterol:40\n Trigliceridos:20\n Hemoglobina:', '2020-08-26', '10:14:40', 5),
(20, 'Analista', 'Log in', 'Correo:covid@19.com', '2020-08-26', '10:23:21', 5),
(21, 'Analista', 'Registro Laboratorio', 'Colesterol:20\n Trigliceridos:10\n Hemoglobina:', '2020-08-26', '10:23:54', 5),
(22, 'Analista', 'Log in', 'Correo:covid@19.com', '2020-08-26', '10:37:00', 5),
(23, 'Analista', 'Registro Laboratorio', 'Colesterol:20\n Trigliceridos:12\n Hemoglobina:', '2020-08-26', '10:37:31', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logcliente`
--

CREATE TABLE `logcliente` (
  `idLogCliente` int(11) NOT NULL,
  `Cliente_idCliente` int(11) NOT NULL,
  `actor` varchar(45) DEFAULT NULL,
  `accion` varchar(45) DEFAULT NULL,
  `datos` varchar(300) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `hora` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `logcliente`
--

INSERT INTO `logcliente` (`idLogCliente`, `Cliente_idCliente`, `actor`, `accion`, `datos`, `fecha`, `hora`) VALUES
(2, 1, 'Cliente', 'Log in', 'Correo:maria@gmail.com', '2020-08-24', '23:04:05'),
(3, 1, 'Cliente', 'Log in', 'Correo:maria@gmail.com', '2020-08-24', '23:07:56'),
(4, 1, 'Cliente', 'Registro Cita', 'Fecha:2020-08-26\n Hora:13:00\n Medico:1', '2020-08-24', '23:08:16'),
(6, 1, 'Cliente', 'Log in', 'Correo:maria@gmail.com', '2020-08-25', '10:46:21'),
(7, 1, 'Cliente', 'Registro Cita', 'Fecha:2020-08-27\n Hora:13:00\n Medico:1', '2020-08-25', '10:46:41'),
(8, 1, 'Cliente', 'Registro Cita', 'Fecha:2020-08-27\n Hora:13:00\n Medico:1', '2020-08-25', '10:49:04'),
(9, 1, 'Cliente', 'Log in', 'Correo:maria@gmail.com', '2020-08-25', '19:25:30'),
(10, 1, 'Cliente', 'Log in', 'Correo:maria@gmail.com', '2020-08-25', '22:41:42'),
(13, 1, 'Cliente', 'Log in', 'Correo:maria@gmail.com', '2020-08-25', '22:55:31'),
(16, 6, 'Cliente', 'Log in', 'Correo:ana@gmail.com', '2020-08-25', '23:02:41'),
(17, 6, 'Cliente', 'Registro Cita', 'Fecha:2020-08-29\n Hora:14:00\n Medico:3', '2020-08-25', '23:02:53'),
(23, 9, 'Cliente', 'Log in', 'Correo:daniela@gmail.com', '2020-08-26', '10:02:24'),
(24, 9, 'Cliente', 'Registro Cita', 'Fecha:2020-08-28\n Hora:10:00\n Medico:1', '2020-08-26', '10:02:46'),
(25, 10, 'Cliente', 'Log in', 'Correo:camila@gmail.com', '2020-08-26', '10:11:52'),
(26, 10, 'Cliente', 'Registro Cita', 'Fecha:2020-08-28\n Hora:12:00\n Medico:1', '2020-08-26', '10:12:15'),
(27, 10, 'Cliente', 'Log in', 'Correo:camila@gmail.com', '2020-08-26', '10:17:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logmedico`
--

CREATE TABLE `logmedico` (
  `idLogMedico` int(11) NOT NULL,
  `Medico_idMedico` int(11) NOT NULL,
  `actor` varchar(45) DEFAULT NULL,
  `accion` varchar(45) DEFAULT NULL,
  `datos` varchar(200) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `hora` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `logmedico`
--

INSERT INTO `logmedico` (`idLogMedico`, `Medico_idMedico`, `actor`, `accion`, `datos`, `fecha`, `hora`) VALUES
(1, 1, 'Medico', 'Log in', 'Correo:armando@gmail.com', '2020-08-24', '23:07:41'),
(2, 1, 'Medico', 'Log in', 'Correo:armando@gmail.com', '2020-08-24', '23:11:22'),
(3, 1, 'Medico', 'Log in', 'Correo:armando@gmail.com', '2020-08-25', '14:15:16'),
(4, 1, 'Medico', 'Log in', 'Correo:armando@gmail.com', '2020-08-25', '22:31:23'),
(5, 3, 'Medico', 'Log in', 'Correo:jorge@gmail.com', '2020-08-25', '23:04:13'),
(6, 3, 'Medico', 'Registrar en Historia', 'Motivo:Nauseas\n Antecedentes:Dolor de estomago\n Observaciones:Puede tener cancer\nFecha:2020-08-25\nHora:23:04:43', '2020-08-25', '23:04:43'),
(7, 2, 'Medico', 'Log in', 'Correo:andres@gmail.com', '2020-08-25', '23:13:23'),
(8, 2, 'Medico', 'Registrar en Historia', 'Motivo:fghfg\n Antecedentes:adfg\n Observaciones:adsgag\nFecha:2020-08-25\nHora:23:13:40', '2020-08-25', '23:13:40'),
(9, 1, 'Medico', 'Log in', 'Correo:armando@gmail.com', '2020-08-26', '10:03:19'),
(10, 1, 'Medico', 'Registrar en Historia', 'Motivo:Dolor de cabeza\n Antecedentes:Migraña\n Observaciones:Se encuentra bien\nFecha:2020-08-26\nHora:10:03:54', '2020-08-26', '10:03:54'),
(11, 1, 'Medico', 'Log in', 'Correo:armando@gmail.com', '2020-08-26', '10:12:42'),
(12, 1, 'Medico', 'Registrar en Historia', 'Motivo:Dolor de estomago\n Antecedentes:Ninguno\n Observaciones:El paciente esta bien\nFecha:2020-08-26\nHora:10:13:17', '2020-08-26', '10:13:17'),
(13, 1, 'Medico', 'Log in', 'Correo:armando@gmail.com', '2020-08-26', '10:17:27'),
(14, 1, 'Medico', 'Log in', 'Correo:armando@gmail.com', '2020-08-26', '10:22:21'),
(15, 1, 'Medico', 'Registrar en Historia', 'Motivo:Dolor de pierna\n Antecedentes:Lesion\n Observaciones:esta bien\nFecha:2020-08-26\nHora:10:22:45', '2020-08-26', '10:22:45'),
(16, 1, 'Medico', 'Log in', 'Correo:armando@gmail.com', '2020-08-26', '10:36:03'),
(17, 1, 'Medico', 'Registrar en Historia', 'Motivo:Dolor de pierna\n Antecedentes:Lesion\n Observaciones:Se lesiono\nFecha:2020-08-26\nHora:10:36:24', '2020-08-26', '10:36:24'),
(18, 1, 'Medico', 'Log in', 'Correo:armando@gmail.com', '2020-08-26', '10:40:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medico`
--

CREATE TABLE `medico` (
  `idMedico` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Apellido` varchar(45) DEFAULT NULL,
  `especialidad` varchar(45) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `clave` varchar(45) DEFAULT NULL,
  `tarjetaPro` varchar(45) DEFAULT NULL,
  `foto` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `medico`
--

INSERT INTO `medico` (`idMedico`, `Nombre`, `Apellido`, `especialidad`, `estado`, `correo`, `clave`, `tarjetaPro`, `foto`) VALUES
(1, 'Armando', 'Castañeda', 'Medico General', 1, 'armando@gmail.com', '202cb962ac59075b964b07152d234b70', '1234569687', 'img/1598328400.jpg'),
(2, 'Andres', 'Melendez', 'Medico General', 1, 'andres@gmail.com', '202cb962ac59075b964b07152d234b70', '312342317', ''),
(3, 'Jorge', 'Melendez', 'Medico General', 1, 'jorge@gmail.com', '202cb962ac59075b964b07152d234b70', '3453123434', 'img/1598414481.jpg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`idAdministrador`);

--
-- Indices de la tabla `analista_clinico`
--
ALTER TABLE `analista_clinico`
  ADD PRIMARY KEY (`idAnalista`);

--
-- Indices de la tabla `cita`
--
ALTER TABLE `cita`
  ADD PRIMARY KEY (`idCita`,`Cliente_idCliente`,`Medico_idMedico`),
  ADD KEY `fk_Medico_has_Cliente_Cliente1_idx` (`Cliente_idCliente`),
  ADD KEY `fk_Medico_has_Cliente_Medico_idx` (`Medico_idMedico`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idCliente`);

--
-- Indices de la tabla `historia`
--
ALTER TABLE `historia`
  ADD PRIMARY KEY (`idHistoria`,`Cliente_idCliente`),
  ADD KEY `fk_Historia_Cliente1_idx` (`Cliente_idCliente`);

--
-- Indices de la tabla `laboratorio_sangre`
--
ALTER TABLE `laboratorio_sangre`
  ADD PRIMARY KEY (`idLaboratorio`,`Cliente_idCliente`,`Analista_Clinico_idAnalista`,`Cita_idCita`),
  ADD KEY `fk_Laboratorio_Sangre_Cliente1_idx` (`Cliente_idCliente`),
  ADD KEY `fk_Laboratorio_Sangre_Analista_Clinico1_idx` (`Analista_Clinico_idAnalista`),
  ADD KEY `fk_Laboratorio_Sangre_Cita1_idx` (`Cita_idCita`);

--
-- Indices de la tabla `logadministrador`
--
ALTER TABLE `logadministrador`
  ADD PRIMARY KEY (`idLogAdministrador`,`Administrador_idAdministrador`),
  ADD KEY `fk_LogAdministrador_Administrador1_idx` (`Administrador_idAdministrador`);

--
-- Indices de la tabla `loganalista_clinico`
--
ALTER TABLE `loganalista_clinico`
  ADD PRIMARY KEY (`idAnalista_Clinico`,`Analista_Clinico_idAnalista`),
  ADD KEY `fk_Analista_Clinico_Analista_Clinico1_idx` (`Analista_Clinico_idAnalista`);

--
-- Indices de la tabla `logcliente`
--
ALTER TABLE `logcliente`
  ADD PRIMARY KEY (`idLogCliente`,`Cliente_idCliente`),
  ADD KEY `fk_LogCliente_Cliente1_idx` (`Cliente_idCliente`);

--
-- Indices de la tabla `logmedico`
--
ALTER TABLE `logmedico`
  ADD PRIMARY KEY (`idLogMedico`,`Medico_idMedico`),
  ADD KEY `fk_LogMedico_Medico1_idx` (`Medico_idMedico`);

--
-- Indices de la tabla `medico`
--
ALTER TABLE `medico`
  ADD PRIMARY KEY (`idMedico`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `idAdministrador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `analista_clinico`
--
ALTER TABLE `analista_clinico`
  MODIFY `idAnalista` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `cita`
--
ALTER TABLE `cita`
  MODIFY `idCita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idCliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `historia`
--
ALTER TABLE `historia`
  MODIFY `idHistoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `laboratorio_sangre`
--
ALTER TABLE `laboratorio_sangre`
  MODIFY `idLaboratorio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `logadministrador`
--
ALTER TABLE `logadministrador`
  MODIFY `idLogAdministrador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT de la tabla `loganalista_clinico`
--
ALTER TABLE `loganalista_clinico`
  MODIFY `idAnalista_Clinico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `logcliente`
--
ALTER TABLE `logcliente`
  MODIFY `idLogCliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `logmedico`
--
ALTER TABLE `logmedico`
  MODIFY `idLogMedico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `medico`
--
ALTER TABLE `medico`
  MODIFY `idMedico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cita`
--
ALTER TABLE `cita`
  ADD CONSTRAINT `fk_Medico_has_Cliente_Cliente1` FOREIGN KEY (`Cliente_idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Medico_has_Cliente_Medico` FOREIGN KEY (`Medico_idMedico`) REFERENCES `medico` (`idMedico`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `historia`
--
ALTER TABLE `historia`
  ADD CONSTRAINT `fk_Historia_Cliente1` FOREIGN KEY (`Cliente_idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `laboratorio_sangre`
--
ALTER TABLE `laboratorio_sangre`
  ADD CONSTRAINT `fk_Laboratorio_Sangre_Analista_Clinico1` FOREIGN KEY (`Analista_Clinico_idAnalista`) REFERENCES `analista_clinico` (`idAnalista`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Laboratorio_Sangre_Cita1` FOREIGN KEY (`Cita_idCita`) REFERENCES `cita` (`idCita`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Laboratorio_Sangre_Cliente1` FOREIGN KEY (`Cliente_idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `logadministrador`
--
ALTER TABLE `logadministrador`
  ADD CONSTRAINT `fk_LogAdministrador_Administrador1` FOREIGN KEY (`Administrador_idAdministrador`) REFERENCES `administrador` (`idAdministrador`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `loganalista_clinico`
--
ALTER TABLE `loganalista_clinico`
  ADD CONSTRAINT `fk_Analista_Clinico_Analista_Clinico1` FOREIGN KEY (`Analista_Clinico_idAnalista`) REFERENCES `analista_clinico` (`idAnalista`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `logcliente`
--
ALTER TABLE `logcliente`
  ADD CONSTRAINT `fk_LogCliente_Cliente1` FOREIGN KEY (`Cliente_idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `logmedico`
--
ALTER TABLE `logmedico`
  ADD CONSTRAINT `fk_LogMedico_Medico1` FOREIGN KEY (`Medico_idMedico`) REFERENCES `medico` (`idMedico`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
