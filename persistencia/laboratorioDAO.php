<?php

class laboratorioDAO{

  private $idLaboratorio;
  private $IdCliente;
  private $Fecha;
  private $Colesterol;
  private $ob_Colesterol;
  private $Trigliceridos;
  private $ob_Trigliceridos;
  private $Hemoglobina;
  private $ob_Hemoglobina;
  private $Linfositos;
  private $ob_Linfositos;
  private $Glucosa;
  private $ob_Glucosa;
  private $Observaciones;
  private $IdAnalista;
  private $idCita;



    public function laboratorioDAO($idLaboratorio="",$Fecha="",$Colesterol="", $ob_Colesterol="",$Trigliceridos="", $ob_Trigliceridos="",$Hemoglobina="", $ob_Hemoglobina="",$Linfositos="", $ob_Linfositos="",$Glucosa="", $ob_Glucosa="",$Observaciones="", $IdCliente=""
    ,$IdAnalista="",$idCita=""){
      $this -> idLaboratorio = $idLaboratorio;
      $this -> Fecha = $Fecha;
      $this -> Colesterol = $Colesterol;
      $this -> ob_Colesterol = $ob_Colesterol;
      $this -> Trigliceridos = $Trigliceridos;
      $this -> ob_Trigliceridos = $ob_Trigliceridos;
      $this -> Hemoglobina = $Hemoglobina;
      $this -> ob_Hemoglobina = $ob_Hemoglobina;
      $this -> Linfositos = $Linfositos;
      $this -> ob_Linfositos = $ob_Linfositos;
      $this -> Glucosa = $Glucosa;
      $this -> ob_Glucosa = $ob_Glucosa;
      $this -> Observaciones = $Observaciones;
      $this -> IdCliente = $IdCliente;
      $this -> IdAnalista = $IdAnalista;
      $this -> idCita= $idCita;
    }

    public function registrar(){
        return "insert into laboratorio_sangre (Fecha,Colesterol, ob_Colesterol,Trigliceridos,ob_Trigliceridos,Hemoglobina,ob_Hemoglobina,Linfositos,ob_Linfositos,Glucosa,ob_Glucosa,observaciones,Cliente_idCliente,Analista_Clinico_idAnalista,Cita_idCita)
                values ('" . $this -> Fecha . "', '" . $this -> Colesterol ."', '" . $this -> ob_Colesterol . "', '" . $this -> Trigliceridos ."', '" . $this -> ob_Trigliceridos ."', '" . $this -> Hemoglobina ."'
                , '" . $this -> ob_Hemoglobina ."', '" . $this -> Linfositos . "', '" . $this -> ob_Linfositos . "', '" . $this -> Glucosa . "', '" . $this -> ob_Glucosa . "', '" . $this -> Observaciones . "'
                , '" . $this -> IdCliente . "', '" . $this -> IdAnalista . "', '" . $this -> idCita . "')";
    }

    public function consultarTodos(){
        return "select Cliente_idCliente,Fecha,Colesterol, ob_Colesterol,Trigliceridos,ob_Trigliceridos,Hemoglobina,ob_Hemoglobina,Linfositos,ob_Linfositos,Glucosa,ob_Glucosa,observaciones,Analista_Clinico_idAnalista
                from laboratorio_sangre";
    }

    public function consultarLaboratoriopdf($id){
        return "select idLaboratorio, Fecha,Colesterol, ob_Colesterol,Trigliceridos,ob_Trigliceridos,Hemoglobina,ob_Hemoglobina,Linfositos,ob_Linfositos,Glucosa,ob_Glucosa,observaciones,Cliente_idCliente,Analista_Clinico_idAnalista
                from laboratorio_sangre
                where Cliente_idCliente = '" . $id .  "'";
    }


    public function consultarHistoria(){
        return "select idLaboratorio,Fecha,Colesterol, ob_Colesterol,Trigliceridos,ob_Trigliceridos,Hemoglobina,ob_Hemoglobina,Linfositos,ob_Linfositos,Glucosa,ob_Glucosa,observaciones,Cliente_idCliente,Analista_Clinico_idAnalista
                from laboratorio_sangre
                where Cliente_idCliente  = '" .$this-> IdCliente .  "'";
    }

    public function consultarLaboratorio(){
        return "select Fecha,Colesterol, ob_Colesterol,Trigliceridos,ob_Trigliceridos,Hemoglobina,ob_Hemoglobina,Linfositos,ob_Linfositos,Glucosa,ob_Glucosa,Observaciones,Analista_Clinico_idAnalista
                from laboratorio_sangre
                where idLaboratorio = '". $this -> idLaboratorio ."'";

    }


    public function consultarGrafico(){
        return "select idLaboratorio,Fecha,Colesterol,Trigliceridos,Hemoglobina,Linfositos,Glucosa,Cliente_idCliente
                from laboratorio_sangre
                where Cliente_idCliente  = '" .$this-> IdCliente .  "'";
    }


}

?>
