<?php
require_once "fpdf/fpdf.php";
require_once "logica/cita.php";
require_once "logica/medico.php";
require_once "logica/laboratorio.php";
require_once "logica/analista.php";
require_once "logica/cliente.php";
require_once "logica/historia.php";


$pdf = new FPDF("P", "mm", "Letter");
$pdf -> SetFont("times", "B", 20);
$pdf -> AddPage();
$pdf -> Image('img/pagina2.jpg',0,0,220);
$pdf ->SetXY(0, 0);
$pdf->SetFillColor(0, 191, 255);
$pdf ->Rect(0,0,220,20,'F');

$pdf -> Cell(216, 20, "Historia Medica",0, 2, "C");
$pdf ->Image('img/logo.png');

$pdf->SetFont('times','',16);
$pdf->SetFillColor(0, 191, 255);
$pdf ->Rect(0,265,220,17,'F');

$Cliente = new cliente($_GET["idCliente"]);
$Cliente->consultarPDF();


$pdf -> SetFont("times", "B", 20);
$pdf->SetY(60);
$pdf -> Cell(190, 20, "Datos Personales",0, 2, "C");

$pdf->SetY(55);
$pdf->SetFont('times','',17);
$pdf ->Image($Cliente->getFoto(),65,80,-290);
$pdf ->Ln();

$pdf->SetY(140);
$pdf->SetX(25);
$pdf ->Cell(90,10,"Nombre:".$Cliente->getNombre(),0);
$pdf ->Cell(90,10,"Apellido:".$Cliente->getApellido(),0);
$pdf ->Ln();
$pdf->SetX(25);
$pdf ->Cell(90,10,"Id:".$Cliente->getIdentificacion(),0);
$pdf ->Cell(90,10,"Sexo:".$Cliente->getSexo(),0);
$pdf ->Ln();
$pdf->SetX(25);
$pdf ->Cell(90,10,"RH:".$Cliente->getRh(),0);
$pdf ->Cell(90,10,"Correo:".$Cliente->getCorreo(),0);
$pdf ->Ln();

$Historia = new Historia("","","",$_GET["idCliente"]);
$Historias=$Historia->consultarTodos();


$pdf -> AddPage();
$pdf -> Image('img/pagina2.jpg',0,0,220);
$pdf -> SetFont("times", "B", 20);
$pdf -> Cell(190, 20, "Citas medicas",0, 2, "C");

$i=1;
$pdf->SetFont('times','',15);
$pdf->SetFillColor(235,236,236);
$pdf->SetTextColor(3, 3, 3);

foreach ($Historias as $HistoriaActual){

    $pdf ->Ln();
    $pdf ->Cell(90,10,"Fecha ".$HistoriaActual->getFecha(),1, 0 , 'L', 1);
    $pdf ->Cell(90,10,"Hora ".$HistoriaActual -> getHora(),1, 0 , 'L', 1);
    $pdf ->Ln();
    $pdf->SetFont('Arial','',12);
    $pdf ->Cell(90,10,"Motivo: ".$HistoriaActual->getMotivo_Consulta(),1, 0 , 'L', 1);
    $pdf ->Cell(90,10,"Antecedentes: ".$HistoriaActual -> getAntecedentes(),1, 0 , 'L', 1);
    $pdf ->Ln();
    $pdf->SetFont('Arial','',13);
    $pdf ->Cell(180,20,"Observacion: ".$HistoriaActual -> getObservaciones(),1, 0 , 'L', 1);
    $pdf ->Ln();
    $i++;
}

$Laboratorio = new laboratorio("","","","","","","","","","","","","",$Cliente->getIdCliente());
$Laboratorios=$Laboratorio->consultarHistoria();


$pdf -> AddPage();
$pdf -> Image('img/pagina2.jpg',0,0,220);
$pdf -> SetFont("times", "B", 20);
$pdf -> Cell(190, 30, "Laboratorios Clinicos",0, 2, "C");
$pdf ->Ln();
$i=1;
$pdf->SetFont('Arial','',15);
$pdf->SetFillColor(235,236,236);
$pdf->SetTextColor(3, 3, 3);
$pdf->SetY(36);
$i=1;
foreach ($Laboratorios as $LaboratorioActual){


    $pdf->SetX(40);
    $pdf ->Cell(50,10,"Fecha:",1, 0 , 'C', 1);
    $pdf ->Cell(89,10,$LaboratorioActual->getFecha(),1, 0 , 'C', 1);
    $pdf ->Ln();
    $pdf->SetX(40);
    $pdf ->Cell(50,10,"Colesterol:",1, 0 , 'C', 1);
    $pdf ->Cell(89,10,$LaboratorioActual->getColesterol()."mg/dl",1, 0 , 'C', 1);
    $pdf ->Ln();
    $pdf->SetX(40);
    $pdf ->Cell(139,10,"Observacion:".$LaboratorioActual->getob_Colesterol(),1, 0 , 'C', 1);
    $pdf ->Ln();
    $pdf->SetX(40);
    $pdf ->Cell(50,10,"Trigliceridos:",1, 0 , 'C', 1);
    $pdf ->Cell(89,10,$LaboratorioActual->getTrigliceridos()."mg/dl",1, 0 , 'C', 1);

    $pdf ->Ln();
    $pdf->SetX(40);
    $pdf ->Cell(139,10,"Observacion:".$LaboratorioActual->getob_Trigliceridos(),1, 0 , 'C', 1);

    $pdf ->Ln();
    $pdf->SetX(40);
    $pdf ->Cell(50,10,"Hemoglobina:",1, 0 , 'C', 1);
    $pdf ->Cell(89,10,$LaboratorioActual->getHemoglobina()."mg/dl",1, 0 , 'C', 1);

    $pdf ->Ln();
    $pdf->SetX(40);
    $pdf ->Cell(139,10,"Observacion:".$LaboratorioActual->getob_Hemoglobina(),1, 0 , 'C', 1);

    $pdf ->Ln();
    $pdf->SetX(40);
    $pdf ->Cell(50,10,"Linfositos:",1, 0 , 'C', 1);
    $pdf ->Cell(89,10,$LaboratorioActual->getLinfositos()."mg/dl",1, 0 , 'C', 1);

    $pdf ->Ln();
    $pdf->SetX(40);
    $pdf ->Cell(139,10,"Observacion:".$LaboratorioActual->getob_Linfositos(),1, 0 , 'C', 1);

    $pdf ->Ln();
    $pdf->SetX(40);
    $pdf ->Cell(50,10,"Glucosa:",1, 0 , 'C', 1);
    $pdf ->Cell(89,10,$LaboratorioActual->getGlucosa()."mg/dl",1, 0 , 'C', 1);

    $pdf ->Ln();
    $pdf->SetX(40);
    $pdf ->Cell(139,10,"Observacion:".$LaboratorioActual->getob_Glucosa(),1, 0 , 'C', 1);

    $pdf ->Ln();
    $pdf->SetX(40);
    $pdf ->Cell(50,25,"Observaciones:",1, 0 , 'C', 1);
    $pdf ->Cell(89,25,$LaboratorioActual->getObservaciones(),1, 0 , 'L', 1);
    $pdf ->Ln();
    $pdf ->Ln();
    $i++;
}


$pdf -> Output();



?>
