<?php
session_start();
require_once "logica/administrador.php";
require_once "logica/cliente.php";
require_once "logica/log.php";
require_once "logica/medico.php";
require_once "logica/cita.php";
require_once "logica/analista.php";
require_once "logica/laboratorio.php";
require_once "logica/historia.php";
require_once "logica/medicoGrafica.php";
require_once "logica/FechasGrafica.php";
require_once "logica/SangreGrafica.php";
require_once "logica/GraficaSexo.php";
require_once "logica/Proceso.php";


$pid = "";
if(isset($_GET["pid"])){
    $pid = base64_decode($_GET["pid"]);
}else{
    $_SESSION["id"]="";
}
if(isset($_GET["cerrarSesion"]) || !isset($_SESSION["id"])){
    $_SESSION["id"]="";
}
?>
<html>
<head>
	<link rel="icon" type="image/png" href="img/logo.png" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" ></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>
	<script>
	$(function () {
		  $('[data-toggle="tooltip"]').tooltip()
	})
	</script>
</head>
<body>
    <?php
	$paginasSinSesion = array(
      "presentacion/autenticar.php",
	);

	if(in_array($pid, $paginasSinSesion)){
	    include $pid;
	}else if($_SESSION["id"]!="") {
	    if($_SESSION["rol"] == "Administrador"){
	        include "presentacion/menuAdministrador.php";
	    }else if($_SESSION["rol"] == "Cliente"){
	        include "presentacion/menuCliente.php";
	    }else if($_SESSION["rol"] == "Analista"){
	        include "presentacion/menuAnalista.php";
      }else if($_SESSION["rol"] == "Medico"){
	        include "presentacion/menuMedico.php";
      }
	    include $pid;
	}else{
      include "presentacion/encabezado.php";
	    include "presentacion/inicio.php";
      include "presentacion/footer.php";
	}
	?>

</body>
</html>
